# Trust in people and institutions in the Netherlands

Spiderweb chart showing the amount of trust there is in people and institutions in the Netherlands. Data from 2017.

## About the data (Dutch)

Deze tabel bevat informatie over de mate waarin de Nederlandse bevolking van 15 jaar en ouder vertrouwen heeft in willekeurige andere mensen. Dit vertrouwen in de medemens is vastgesteld door te vragen of andere mensen over het algemeen te vertrouwen zijn of dat je niet voorzichtig genoeg kunt zijn in de omgang met mensen. In de tabel wordt het aandeel getoond dat andere mensen over het algemeen te vertrouwen vindt. Daarbij zijn personen die geen antwoord hebben gegeven, omdat zij het antwoord niet weten of niet willen zeggen, buiten beschouwing gelaten.

Daarnaast bevat de tabel gegevens over het vertrouwen in diverse organisaties en hun functioneren. Het gaat daarbij om de volgende instellingen: kerken, leger, rechters, pers, de politie, de Tweede Kamer, ambtenaren, banken, grote bedrijven en de Europese Unie. Er is gevraagd of de respondent hier heel veel, tamelijk veel, niet zo veel of helemaal geen vertrouwen in heeft. De tabel geeft het aandeel dat heel veel of tamelijk veel vertrouwen in deze organisaties heeft. Ook daarbij zijn personen die geen antwoord hebben gegeven buiten beschouwing gelaten.

## Key points in English

- Dutch population of 15 years and older.
- Question regarding trust in others phrased as "In general, can people be trusted or can you not be careful enough?"
- Included in the percentage of people who have trust in X are those who answered they had "a lot" or "quite a lot" of trust.
- When a question wasn't answered it was omitted from the results (not included in either trusted or mistrusted)

From the [Centraal Bureau voor de Statistiek](https://opendata.cbs.nl/statline/portal.html?_la=nl&_catalog=CBS&tableId=82378NED&_theme=382).